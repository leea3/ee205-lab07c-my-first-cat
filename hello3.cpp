///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07c - My First Cat - EE 205 - Spr 2022
///
/// @note Hello world program using classes
/// @file hello3.cpp
/// @version 1.0
///
/// @author Arthur Lee <leea3@hawaii.edu>
/// @date   03 Mar 2022
///////////////////////////////////////////////////////////////////////////////


#include<iostream>

using namespace std;

class Cat {
public:
   void sayHello() {
   cout << "Meow!" << endl;
   }
};

int main() {
   //instantiates a Cat
   Cat myCat;
   myCat.sayHello();
}
