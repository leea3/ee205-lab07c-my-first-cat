///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07c - My First Cat - EE 205 - Spr 2022
///
/// @note Hello world program using namespace
/// @file hello1.cpp
/// @version 1.0
///
/// @author Arthur Lee <leea3@hawaii.edu>
/// @date   03 Mar 2022
///////////////////////////////////////////////////////////////////////////////

//necessary for cout
#include<iostream>

using namespace std;

int main() {
   cout << "Hello World!" << endl;
}
